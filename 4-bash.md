# Commands to learn
* find - command to find a directory or a file in the specified path.
* grep - allow us to find words or lines in a text file.
* netstat - prints all of the network stats.
* nice - declare the process priority. From -20 (highest priority) to 19 (lowest priority).
* uname - prints info about the computer.
* lsof - stants for "list open files". Lists which file has currently opened by which process.
* sysctl - allow us to change OS variables.
* insmod - allow us to insert modules into the kernel.
* stat - prints stats about a file
* vim - a tool for editing text inside the terminal
* nano - another tool for editing insied the terminal
* less - display a file content on terminal. use when you want to display a long file in parts.
* cp - copy a file to destination
* mv - move or rename a file
* lscpu - show info about the cpu
* watch - runs a command repeatedly to see any output differences in real time
* dmesg - display the kernel ring buffer.
* (A ring buffer) - buffer that hold all the kernel messages.
* free - display the space that is used/unused in the phisical/swap memory
* man - display manual for each bash command.
