# Signals
## 1. What is a signal?
A message sent to a process. Usually to terminate or kill it.
## 2. Name 5 common signals.
SIGKILL - kills the process.
SIGSTOP - - stops the process.
SIGINT - interrupt the process. Can be sent using CTRL+C
## 3. Write a program that cant be killed with ctrl+c and another program to kill it.

# Permissions
## 1. What are these?
allow us to define which user/users can read, write or execute every directory/file.

#General
## 1. What is UNIX?
Family of multiuser and multitasking os.
## 2. What is POSIX?
List of standarts to maintain compabillity between unix like os.

# Syscalls
## 1. What is a syscall?
A simple way a program can interface the kernel.
## 2. What happens when you call a syscall?
It goes directly to the kernel (There might be some wrapper functions between.)
## 3. Exercise this syscalls: (in the last project)

###These next 4 syscalls use file descriptor which is a data storage for a process files (INT).
open - opens a file
write - writes to a file
read - reads a file
close - closes a file

###These next 4 syscalls use the file pointer.
fopen - opens a file
fwrite - writes to a file
fread - reads a file
fclose - closes a file

lseek - set the file offset - cursor

socket - creates an endpoint which can communicate throught the network.

listen - wait for a connection to the socket.
bind - bind a local address to the socket's file descriptor.
mprotect - protect a block of memory (made of pages) by giving it certain permissions - READ,WRITE only e.g.

fstat - returns details about a directory/file. Size, permissions etc..
lstat - uses a symbolic link instead of file path.

symbolic link - a way to reference a file using a global name declared by the user.

fork - creates a duplicate child thread. Make current thread the parent thread. Each has differnent ids.

execve - replaces the current process content with another content (content = program). Id stays the same. The new process uses a different memory area.

uname - prints information about the computer.

chmod - let us set permissions (r-read, w-write, x-execute) to a directory or a file.

nice - nice value is the process priority (of the processor). Nice syscalls allow us to set the process priority from -20 (highest priority) to 19 (lowest priority).

renice - the same as nice but is meant for already existing processes.

pgrep - allow us to find process id using it's properties - name, user etc..

cat - allow us to append, overwrite, print a file.

# Process & Threads
## 1. What is the difference between a process and a thread?
A process can contain many threads. All the threads in a process share the same memory, while processes don't. 
## 2. Explain what does "Copy on write" mean?
It means that when using "fork", the parent process and the child process uses the same memory. When one of the processes want to change the memory, the OS creates a copy of the current page that need to be changed. 
## 3. What is the difference between "execve" and "fork"?
"fork" creates a duplicate child thread of the current process (which has a different id), making the current process the parent process. "exec" replace the current process's content with another content. The process id remain the same.
## 4. What does clone do?
similar to fork - it creates a child thread. The difference is that when using clone, the new process can share some data with the parent process.
## 5. Exercise fork, clone, execve.
