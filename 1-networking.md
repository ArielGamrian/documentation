# Ethernet
## 1. What is the purpose of the protocol?

## 2. What are the main feeds it contains?


# IP

## 1. What is the purpose of the protocol?

## 2. Why do we need both "Ethernet" address and "IP" address?

## 3. what is NAT? What is it good for? (2 main things)

## 4. What is ICMP? Write command for that sends an icmp packet to the ip 192.168.1.1 with a custom content.

## 5. What is DHCP?

## 6. What is DNS?

## 7. What is ARP? Why do we need it?

## exercise:
### We just bought a new compueter and we want to connect it to our LAN at home. We want to ping the ip - 192.168.1.3 The new computer is connected to the same switch and router that we are connected to. Describe the flow of sent packets right from the first connection of the computer to the router and switch and after - the ping command.

# TCP and UDP
## 1. Why do we need them both?
## 2. What is the difference between them?
## 3. Describe a TCP handshake (Also refer to the sequence number).
## 4. Describe the sequence number mechanism in TCP.

# HTTP 
## 1. What is it used for?
## 2. What kind of layers and in which order are HTTP packets made of?
## 3. What is the difference between POST and GET?

# Sockets
## 1. What is a socket?
An endpoint to communicate with other endpoints. Usually on the internet.
## 2. Write a simple program in Python that has server which accepts all clients and prints their messages. All clients should print messages from everyone as well.
## 3. Using netstat - write a command that shows all the TCP server sockets which listen on port 80.

Server: Socket -> Bind -> Listen -> Accept -> RW
Client: Socket -> Connect -> RW
